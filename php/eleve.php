<!DOCTYPE html>
<html>
<head>
	<title>Notes - IUT</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="icon" type="image/jpg" href="/favicon.jpg" />
	<script type="text/javascript" src="/js/functions.js"></script>
	<?php session_start(); ?>
</head>
<body>
	<?php include('functions.php');?>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/html/header.html'); ?>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/php/nav_eleve.php'); ?>
	<div id="content">
		<form action="eleve.php" method="post" id="module">
		<select name="module">
			<?php choixModuleEleve($_SESSION['login']); ?>
		</select>
		<input type="submit" name="envoie" value="Filtrer">
		</form>
		<br>
		<?php
		if (isset($_POST['module']) && !empty($_POST['module'])) {
			afficheNoteEleve($_SESSION['login'],$_POST['module']);
		}
		else{
		afficheNoteEleve($_SESSION['login'],NULL);	
		}
		?>
	</div>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/html/footer.html'); ?>
</body>
</html>