<?php
//Etablissement de la connexion avec la base de donnée
function connexionBDD(){
	try
	{
		$bdd = new PDO('mysql:host=localhost;dbname=notes-iut', 'root', '');
		return($bdd);
	}
	catch (Exception $e)
	{
		die('Erreur : ' . $e->getMessage());
	}
}

function afficheClasse($classe){
	$bdd=connexionBDD();
	if (empty($classe)) {
		$eff_sql=$bdd->query('SELECT * FROM etudiant ORDER BY classe_ID ASC');
	}
	else{
		$eff_sql=$bdd->query('SELECT * FROM etudiant WHERE classe_ID="'.$classe.'" ORDER BY classe_ID ASC');
	}
	echo('
		<table id=tableauNote>
			<tr>
				<td>
					ID
				</td>
				<td>
					Nom
				</td>
				<td>
					Prenom
				</td>
				<td>
					Classe
				</td>
			</tr>
		</table>
		');
	echo "<table id='tableauNote'>";
	foreach ($eff_sql as $eff) {
		echo "<tr>";
		echo "<td>".$eff['etudiant_ID']."</td>";
		echo "<td>".$eff['etudiant_nom']."</td>";
		echo "<td>".$eff['etudiant_prenom']."</td>";
		echo "<td>".$eff['classe_ID']."</td>";
		echo '</table>';
	}
	$eff_sql->closecursor();
}

function afficheClasseAdmin($classe){
	$bdd=connexionBDD();
	if (empty($classe)) {
		$eff_sql=$bdd->query('SELECT * FROM etudiant ORDER BY classe_ID ASC');
	}
	else{
		$eff_sql=$bdd->query('SELECT * FROM etudiant WHERE classe_ID="'.$classe.'" ORDER BY classe_ID ASC');
	}
	echo('
		<table id=tableauNote>
			<tr>
				<td>
					ID
				</td>
				<td>
					Nom
				</td>
				<td>
					Prenom
				</td>
				<td>
					Classe
				</td>
				<td>
					Supprimer
				</td>
			</tr>
		</table>
		');
	echo "<table id='tableauNote'>";
	foreach ($eff_sql as $eff) {
		echo "<tr>";
		echo "<td>".$eff['etudiant_ID']."</td>";
		echo "<td>".$eff['etudiant_nom']."</td>";
		echo "<td>".$eff['etudiant_prenom']."</td>";
		echo "<td>".$eff['classe_ID']."</td>";
		echo '<td> <a href="suppEtu.php?prof=';
		echo $eff['etudiant_ID'];
		echo '">OK</a> </td>';
		echo '</table>';
	}
	$eff_sql->closecursor();
}

function afficheNoteEleve($eleve,$module){
	$bdd = connexionBDD();
	if (empty($module)) {
		$note_sql=$bdd->query('SELECT * FROM note WHERE etudiant_ID="'.$eleve.'"');	
	}
	else{
		$note_sql=$bdd->query('SELECT * FROM note WHERE etudiant_ID="'.$eleve.'" AND matiere_ID="'.$module.'" ');
	}	
	echo('
		<table id=tableauNote>
			<tr>
				<td>
					N° Etudiant
				</td>
				<td>
					Note
				</td>
				<td>
					Module
				</td>
				<td>
					Coeff.
				</td>
			</tr>
		</table>
		');
	echo '<table id="tableauNote">';
	foreach ($note_sql as $note) {
		echo '<tr>';
		echo '<td>'.$note['etudiant_ID'].'</td>';
		echo '<td>'.$note['note'].'</td>';
		echo '<td>'.$note['matiere_ID'].'</td>';
		echo '<td>'.$note['Coeff'].'</td>';
		echo '</tr>';
	}
	echo '</table>';
	$note_sql->closecursor();
}

function afficheNote(){
	$bdd=connexionBDD();
	$note_sql=$bdd->query('SELECT * FROM note');
	echo('
		<table id=tableauNote>
			<tr>
				<td>
					N° Etudiant
				</td>
				<td>
					Note
				</td>
				<td>
					Module
				</td>
				<td>
					Coeff.
				</td>
			</tr>
		</table>
		');
	echo '<table id="tableauNote">';
	foreach ($note_sql as $note) {
		echo '<tr>';
		echo '<td>'.$note['etudiant_ID'].'</td>';
		echo '<td>'.$note['note'].'</td>';
		echo '<td>'.$note['matiere_ID'].'</td>';
		echo '<td>'.$note['Coeff'].'</td>';
		echo '</tr>';
	}
	echo '</table>';
	$note_sql->closecursor();
}

function afficheNoteProf(){
	$bdd=connexionBDD();
	$note_sql=$bdd->query('SELECT * FROM note');
	echo('
		<table id=tableauNote>
			<tr>
				<td>
					N° Etudiant
				</td>
				<td>
					Note
				</td>
				<td>
					Module
				</td>
				<td>
					Coeff.
				</td>
				<td>
					Supprimer
				</td>
			</tr>
		</table>
		');
	echo '<table id="tableauNote">';
	foreach ($note_sql as $note) {
		echo '<tr>';
		echo '<td>'.$note['etudiant_ID'].'</td>';
		echo '<td>'.$note['note'].'</td>';
		echo '<td>'.$note['matiere_ID'].'</td>';
		echo '<td>'.$note['Coeff'].'</td>';
		echo '<td> <a href="suppnote.php?note=';
		echo $note['note_ID'];
		echo '"> OK </a> </td>';
		echo '</tr>';
	}
	echo '</table>';
	$note_sql->closecursor();
}

function afficheProf(){
	$bdd=connexionBDD();
	$eff_sql=$bdd->query('SELECT * FROM prof');
	echo('
		<table id=tableauProf>
			<tr>
				<td>
					ID
				</td>
				<td>
					Prenom
				</td>
				<td>
					Nom
				</td>
			</tr>
		</table>
		');
	echo "<table id='tableauNote'>";
	foreach ($eff_sql as $eff) {
		echo "<tr>";
		echo "<td>".$eff['prof_ID']."</td>";
		echo "<td>".$eff['prof_prenom']."</td>";
		echo "<td>".$eff['prof_nom']."</td>";
		echo "</td>";
	}
	echo '</table>';
	$eff_sql->closecursor();
}

function afficheProfAdmin(){
	$bdd=connexionBDD();
	$eff_sql=$bdd->query('SELECT * FROM prof');
	echo('
		<table id=tableauProf>
			<tr>
				<td>
					ID
				</td>
				<td>
					Prenom
				</td>
				<td>
					Nom
				</td>
				<td>
					Supprimer
				</td>
			</tr>
		</table>
		');
	echo "<table id='tableauNote'>";
	foreach ($eff_sql as $eff) {
		echo "<tr>";
		echo "<td>".$eff['prof_ID']."</td>";
		echo "<td>".$eff['prof_prenom']."</td>";
		echo "<td>".$eff['prof_nom']."</td>";
		echo "</td>";
		echo '<td> <a href="suppProf.php?prof=';
		echo $eff['prof_ID'];
		echo '">OK</a> </td>';
	}
	echo '</table>';
	$eff_sql->closecursor();
}

function choixModuleEleve($eleve){
	$bdd=connexionBDD();
	$mod_sql=$bdd->query('SELECT * FROM note WHERE etudiant_ID="'.$eleve.'"');
	foreach ($mod_sql as $mod) {
		echo "<option>".$mod['matiere_ID']."</option>";
	}
}

function choixModule(){
	$bdd=connexionBDD();
	$mod_sql=$bdd->query('SELECT * FROM note');
	foreach ($mod_sql as $mod) {
		echo "<option>".$mod['matiere_ID']."</option>";
	}
}

function choixClasse(){
	$bdd=connexionBDD();
	$mod_sql=$bdd->query('SELECT * FROM classe');
	foreach ($mod_sql as $mod) {
		echo "<option>".$mod['classe_ID']."</option>";
	}
}

function choixEleve(){
	$bdd=connexionBDD();
	$mod_sql=$bdd->query('SELECT * FROM etudiant');
	foreach ($mod_sql as $mod) {
		echo "<option value='".$mod['etudiant_ID']."''>".$mod['etudiant_ID']."-".$mod['etudiant_nom']." ".$mod['etudiant_prenom']." ".$mod['classe_ID']."</option>";
	}
}

function moyenneModule($eleve,$module){
	$bdd=connexionBDD();
	if (empty($module)) {
		$moy_sql=$bdd->query('SELECT * FROM note WHERE etudiant_ID="'.$eleve.'"');
		echo('
			<table id=tableauNote>
				<tr>
					<td>
						N° Etudiant
					</td>
					<td>
						Moyenne
					</td>
					<td>
						Module
					</td>
					<td>
						Coeff.
					</td>
				</tr>
			</table>
			');
	}
	else{
		$moy_sql=$bdd->query('SELECT * FROM note WHERE etudiant_ID="'.$eleve.'" AND matiere_ID="'.$module.'"');
		echo('
			<table id=tableauNote>
				<tr>
					<td>
						N° Etudiant
					</td>
					<td>
						Moyenne
					</td>
					<td>
						Module
					</td>
					<td>
						Coeff.
					</td>
				</tr>
			</table>
			');
		$sum=0;
		$div=0;
		foreach ($moy_sql as $moy) {
			$sum=$sum+$moy['note']*$moy['Coeff'];
			$div=$div+$moy['Coeff'];
		}
		echo "<table id='tableauNote'>";
		echo "<tr>";
		echo "<td>";
		echo $eleve;
		echo "</td>";
		echo "<td>";
		echo $sum/$div;
		echo "</td>";
		echo "<td>";
		echo $module;
		echo "</td>";
		echo "<td>";
		echo "2";
		echo "</td>";
		echo "</tr>";
		echo "</table>";
	}

}

function rajoutProf($id,$nom,$prenom){
	$bdd=connexionBDD();
	$bdd->exec('INSERT INTO prof (prof_ID,prof_nom,prof_prenom,password) VALUES("'.$id.'","'.$nom.'","'.$prenom.'","root")');
}

function rajoutEleve($id,$nom,$prenom){
	$bdd=connexionBDD();
	$bdd->exec('INSERT INTO `notes-iut`.`etudiant` (`etudiant_ID`, `password`, `etudiant_nom`, `etudiant_prenom`,`classe_ID`) VALUES ("'.$id.'", "'.$nom+$prenom.'", "'.$nom.'", "'.$prenom.'", "'.$classe.'"');
}

function rajoutModule($m,$nom){
	$bdd=connexionBDD();
	$bdd->exec('INSERT INTO matiere (matiere_M,matiere_nom) VALUES ("'.$m.'","'.$nom.'")');
}

function rajoutNote($note,$etu,$m,$coeff,$prof){
	$bdd=connexionBDD();
	$bdd->exec('INSERT INTO note (note,etudiant_ID,matiere_ID,Coeff,prof_ID) VALUES ("'.$note.'","'.$etu.'","'.$m.'","'.$$coeff.'","'.$prof.'")');
}

function supprimerNote($note){
	$bdd=connexionBDD();
	$bdd->exec('DELETE FROM note WHERE note_ID="'.$note.'"');
}

function supprimerProf($prof){
	$bdd=connexionBDD();
	$bdd->exec('DELETE FROM prof WHERE prof_ID="'.$prof.'"');
}

function supprimerEleve($etu){
	$bdd=connexionBDD();
	$bdd->exec('DELETE FROM etudiant WHERE etudiant_ID="'.$etu.'"');
	$bdd->exec('DELETE FROM note WHERE etudiant_ID="'.$etu.'"');
}

function deco(){
	session_start();
	$_SESSION = array();
	session_destroy();
	header('location:/index.php');
}


?>