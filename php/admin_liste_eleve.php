<!DOCTYPE html>
<html>
<head>
	<title>Notes - IUT</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="icon" type="image/jpg" href="/favicon.jpg" />
	<script type="text/javascript" src="/js/functions.js"></script>
	<?php session_start(); ?>
</head>
<body>
	<?php include('functions.php');?>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/html/header.html'); ?>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/php/admin_nav.php'); ?>
	<div id="content">
		<form action="admin_liste_eleve.php" method="post" id="module">
			<select name="classe">
				<?php choixClasse(); ?>
			</select>
			<input type="submit" name="envoie" value="Filtrer">
		</form>
		<br>
		<?php
		if (empty($_POST['classe'])) {
			afficheClasseAdmin(NULL);
		}
		else{
			afficheClasseAdmin($_POST['classe']);	
		}
		?>
	</div>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/html/footer.html'); ?>
</body>
</html>