<!DOCTYPE html>
<html>
<head>
	<title>Notes - IUT</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="icon" type="image/jpg" href="/favicon.jpg" />
	<script type="text/javascript" src="/js/functions.js"></script>
	<?php session_start(); ?>
</head>
<body>
	<?php include('functions.php');?>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/html/header.html'); ?>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/php/prof_nav.php'); ?>
	<div id="content">
		<form action="rajoutNote.php" method="post" id="login">
			<h3>Notes :</h3>
			<p>Eleve :
				<select name="eleve">
					<?php choixEleve();?>
				</select>
			</p>
			<p>Note :
			<input type="number" min="0" max="20" name="note"></p>
			<p>Module
				<select name="module"><?php choixModule();?></select>
			</p>
			<p>Coeff:
				<input type="number" min="0.5" max="3" step="0.5" name="coeff">
			</p>
			<input type="submit" value="Rajouter">
			<br><br>
			<?php 
					if (isset($_GET['err1'])) {
						echo ' Rajout confirmé.';
					}
					?>
		</form>
	</div>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/html/footer.html'); ?>
</body>
</html>