<!DOCTYPE html>
<html>
<head>
	<title>Notes - IUT</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="/css/style.css">
	<link rel="icon" type="image/jpg" href="/favicon.jpg" />
	<script type="text/javascript" src="/js/functions.js"></script>
	<?php session_start(); ?>
</head>
<body>
	<?php include('functions.php');?>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/html/header.html'); ?>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/php/admin_nav.php'); ?>
	<div id="content">
		<form action="rajoutProf.php" method="post" id="login">
			<h3>Prof :</h3>
			<p>Login :
			<input type="text" name="login_prof"></p>
			<p>
				Nom :<input type="text" name="nom_prof">
			</p>
			<p>
				Prenom :<input type="text" name="prenom_prof">
			</p>
			<input type="submit" value="Rajouter">
			<br><br>
			<?php 
					if (isset($_GET['err1'])) {
						echo ' Rajout confirmé.';
					}
					?>
		</form>
		<br>
		<form action="rajoutEleve.php" method="post" id="login">
			<h3>Eleve :</h3>
			<p>Login :
			<input type="text" name="login_eleve"></p>
			<p>
				Nom :<input type="text" name="nom_eleve">
			</p>
			<p>
				Prenom :<input type="text" name="prenom_eleve">
			</p>
			<p>
				Classe : <select name="classe">
				<?php choixClasse(); ?>
			</select>
			</p>
			<p>
				Date de naissance :<input type="date" name="naiss_eleve">
			</p>
			<input type="submit" value="Rajouter">
			<br><br>
			<?php 
					if (isset($_GET['err2'])) {
						echo ' Rajout confirmé.';
					}
					?>
		</form>
		<br>
		</form>
		<br>
		<form action="rajoutModule.php" method="post" id="login">
			<h3>Module :</h3>
			<p>Id de matière :
			<input type="text" name="matiere_ID"></p>
			<p>
				Nom matière :<input type="text" name="matiere_nom">
			</p>
			<input type="submit" value="Rajouter">
			<br><br>
			<?php 
					if (isset($_GET['err3'])) {
						echo ' Rajout confirmé.';
					}
					?>
		</form>
	</div>
	<?php include($_SERVER['DOCUMENT_ROOT'].'/html/footer.html'); ?>
</body>
</html>