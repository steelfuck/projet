<?php
//Inclusion des fonctions PHP
include('functions.php');

//Vérification du formulaire
if(isset($_POST['login']) && !empty($_POST['login']) && isset($_POST['password']) && !empty($_POST['password'])){

	//Connexion à la base de données
	$bdd=connexionBDD();

	//Récupération des données des élèves
	$log_mysql = $bdd->query('SELECT * FROM etudiant  WHERE etudiant_ID="'.$_POST['login'].' " ');
	$log_etu = $log_mysql->fetch();

	//Récupération des données des professeurs
	$log_mysql = $bdd->query('SELECT * FROM prof WHERE prof_ID="'.$_POST['login'].'"');
	$log_prof = $log_mysql->fetch();

	//Récupération des données des membres de l'administration
	$log_mysql = $bdd->query('SELECT * FROM admin WHERE admin_ID="'.$_POST['login'].'"');
	$log_admin = $log_mysql->fetch();

	//Test de connexion pour étudiants
	if($_POST['login'] == $log_etu['etudiant_ID'] && $_POST['password'] == $log_etu['password']){
		session_start();
		$_SESSION['login'] = $_POST['login'];
		$_SESSION['nom'] = $log_etu['etudiant_nom'];
		$_SESSION['prenom'] = $log_etu['etudiant_prenom'];
		$_SESSION['classe'] = $log_etu['classe_ID'];
		$_SESSION['prio'] = 1;
		header('location:eleve.php');
	}

	//Test de connexion pour professeurs
	elseif ($_POST['login'] == $log_prof['prof_ID'] && $_POST['password'] == $log_prof['password']) {
		session_start();
		$_SESSION['login'] = $_POST['login'];
		$_SESSION['nom'] = $log_prof['prof_nom'];
		$_SESSION['prenom'] = $log_prof['prof_prenom'];
		$_SESSION['prio'] = 2;
		header('location:prof.php');
	}

	//Test de connexion pour membres de l'administration
	elseif ($_POST['login'] == $log_admin['admin_ID'] && $_POST['password'] == $log_admin['password']) {
		session_start();
		$_SESSION['login'] = $_POST['login'];
		$_SESSION['nom'] = $log_admin['admin_nom'];
		$_SESSION['prenom'] = $log_admin['admin_prenom'];
		$_SESSION['prio'] = 3;
		header('location:admin.php');
	}
	//Renvoie à la page de conexion si identifiants faux
	else{
		header('location:/index.php?err=OK');
	}
}
//Renvoie à la page de connexion si formulaire vide
else{
	header('location:/index.php?err=OK');
}
?>