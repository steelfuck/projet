<!DOCTYPE html>
<html>
<head>
	<title>Notes - IUT</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="icon" type="image/jpg" href="favicon.jpg" />
	<script type="text/javascript" src="js/functions.js"></script>
</head>
<body>
	<?php include('php/functions.php');?>
	<?php include('html/header.html'); ?>
	<div id="content">
		<form action="php/login.php" method="post" id="login">
			<h3>Login :</h3>
			<p>Identifiants :<br>
				<input type="text" name="login" placeholder="Votre identifiant"></p>
				<p>Mot de passe :<br>
					<input type="password" name="password" placeholder="Votre mot de passe"></p>
					<br>
					<input type="image" name="submit" src="css/submit.png">
					<?php 
					if (isset($_GET['err'])) {
						echo '<br>';
						echo ' Authentification incorrecte.<br>Veuillez recommencer.';
					}
					?>	
				</form>
			</div>
			<?php include('html/footer.html'); ?>
		</body>
		</html>